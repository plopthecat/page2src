require 'slop'

opts = Slop.parse do |o|
  o.string '-p', '--pages-url', required: true
  o.on '--version', 'print the version' do
    puts Slop::VERSION
    exit
  end
end

ARGV #=> -v --login alice --host 192.168.0.1 --check-ssl-certificate

